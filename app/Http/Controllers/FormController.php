<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function akun(){
        return view('form');
    }

    public function signup(Request $request){
        $fname = $request['fname'];
        $lname = $request['lname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language1 = $request['language1'];
        $language2 = $request['language2'];
        $language3 = $request['language3'];
        $bio = $request['bio'];
        return view('welcome', compact('fname','lname','gender','nationality','language1','language2','language3','bio'));
    }
}
