<!DOCTYPE html>
<html>

<head>
    <title>Sign Up Form</title>
</head>

<body>

    <h1>Buat Akun Baru</h1>

    <form action="welcome" method="post">
        @csrf
            <label for="fname">First name:</label><br><br>
            <input type="text" id="fname" name="fname" value=""><br><br>
            <label for="lname">Last name:</label><br><br>
            <input type="text" id="lname" name="lname" value=""><br><br>
            <p>Gender:</p>
                <input type="radio" id="male" name="gender" value="Male">
                <label for="male">Male</label><br>
                <input type="radio" id="female" name="gender" value="Female">
                <label for="female">Female</label><br>
                <input type="radio" id="other" name="gender" value="Other">
                <label for="other">Other</label><br><br>
            <label for="nationality">Nationality:</label><br><br>
                <select id="nationality" name="nationality">
                  <option value="Indonesia">Indonesia</option>
                  <option value="Amerika">Amerika</option>
                  <option value="Inggris">Inggris</option>
                </select><br><br>
            <p>Language Spoken:</p>
                <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
                <label for="language1"> Bahasa Indonesia</label><br>
                <input type="checkbox" id="language2" name="language2" value="English">
                <label for="language2"> English</label><br>
                <input type="checkbox" id="language3" name="language3" value="Other">
                <label for="language3"> Other</label>
            <br><br>
            <p>Bio</p>
                <textarea name="bio" rows="10" cols="30"></textarea>
                <br>
        <input type="submit" value="Sign Up">
    </form>
    
</body>
</html>