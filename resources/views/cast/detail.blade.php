@extends('layout.master')

@section('judul')
    Detail Pemeran {{$cast->nama}}
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{$cast->nama}} ({{$cast->umur}} tahun)
        </div>
        <div class="card-body">
            <h5 class="card-title">Biodata</h5>
            <p class="card-text">{{$cast->bio}}</p>
            <a href="/cast" class="btn btn-dark">Kembali</a>
        </div>
    </div>
@endsection