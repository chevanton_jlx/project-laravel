@extends('layout.master')

@section('judul')
    Edit Pemeran {{$cast->nama}}
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama Pemeran</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control" name="umur" value="{{$cast->umur}}">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Biodata</label>
            <textarea class="form-control" name="bio">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Update</button>
        <a href="/cast" class="btn btn-dark">Batal</a>
    </form>
@endsection